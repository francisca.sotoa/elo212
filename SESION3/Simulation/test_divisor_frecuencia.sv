`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.05.2021 23:51:37
// Design Name: 
// Module Name: test_divisor_frecuencia
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_divisor_frecuencia(
   

);
    logic clk_in,reset,clk_out_50M,clk_out_30M,clk_out_10M,clk_out_1M;
    //logic [7:0] count;//?
    modulos_divisor_frecuencia DUT(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out_50M(clk_out_50M),
            .clk_out_30M(clk_out_30M),
            .clk_out_10M(clk_out_10M),
            .clk_out_1M(clk_out_1M),
            .reset(reset)
            
        );
    
    
      
    
    always #1 clk_in = ~clk_in; //Generacion senal de reloj
    //assign BCD_in=count;
    initial begin
        clk_in=1;
        reset=1;
        #10 reset =0;
        #1 reset=1;
        #5 reset=0;
        #1 reset=1;
        #1 reset=0;
        #15 reset=1;
        #10 reset =0;
        #14 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #11 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #13 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        
    end
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2021 00:07:58
// Design Name: 
// Module Name: test_modulos_multiplex_temporal
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_modulos_multiplex_temporal();
        //logic clk_in,reset;
        logic clk_in,reset1,reset2;
        logic [6:0] multiplexor_sevenSeg;
        logic [3:0] posicion_sevenSeg;
        
        
    modulos_multiplex_temporal DUT(
            .clk_in(clk_in),//instanciar entradas y salidas
            //.reset(reset),
            .multiplexor_sevenSeg(multiplexor_sevenSeg),
            .posicion_sevenSeg(posicion_sevenSeg),
            .reset1(reset1),
            .reset2(reset2)
            
            
            
            
        );
    
    
      
    
    always #1 clk_in = ~clk_in; //Generacion senal de reloj
    
    initial begin
        clk_in=1;
        reset1=0;
        reset2 =0;
        
        #2 reset2=1;
        #5 reset1=1;
        #5 reset1=0;
        #5 reset2=0;
        
        
        
        
        
        
        
        
        
        /*5 reset=0;
        #1 reset=1;
        #1 reset=0;
        #15 reset=1;
        #10 reset =0;
        #14 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #11 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #13 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;*/
        
    end
    
    

endmodule

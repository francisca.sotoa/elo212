`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.05.2021 23:02:12
// Design Name: 
// Module Name: test_clock_divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_clock_divider();
    logic clk_in,reset,clk_out;
    
    clock_divider #(
            .COUNTER_MAX(8)
            
            
        )DUT(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out),
            .reset(reset)
            
        );
        
    always #1 clk_in = ~clk_in; //Generacion senal de reloj
    //always #15 reset = ~reset;
    initial begin
        clk_in=1;
        reset=1;
        #10 reset =0;
        #15 reset=1;
        #5 reset=0;
        #1 reset=1;
        #1 reset=0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        #15 reset=1;
        #10 reset =0;
        
        
        
    end
        
        
        
endmodule

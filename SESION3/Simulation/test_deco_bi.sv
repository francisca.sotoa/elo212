`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.05.2021 00:34:13
// Design Name: 
// Module Name: test_deco_bi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_deco_bi();
    logic [1:0] A;//definicion de conexiones virtuales
    logic [1:0]sel;
    logic [3:0]out;
    
    deco_bi DUT( //instancia del modulo a testear
        .A(A),
        .sel(sel),
        .out(out)
        );
    
    initial begin //aca se asignan valores de prueba o estimulos
        /*
        Posibles valores de sel
        00 : out=A;
        01 : out=B;      
        10 : out=C;
        11 : out=D;
        */
        A=2'b00; 
        #3 
        A=2'b01;//1 en binario
        #3
        A=2'b10;//3 en binario, 4'b0011 es equivalente a 4'd3
        #3
        A=2'b11;//7 en binario
        
        
     end
        
        
        
      
       
        
    
endmodule

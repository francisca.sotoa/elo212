`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.05.2021 23:43:12
// Design Name: 
// Module Name: reloj_sincronico_test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reloj_sincronico_test();
    logic clk,reset;
    logic [3:0] Q,D;//Q es contador
    
    reloj_sincronico DUT (
        .clk(clk),
        .reset(reset),
        .Q(Q),
        .D(D)
        
    
    );
    
    always #6 clk=~clk;//Generacion se�al de reloj
    always #5 reset=~reset;
    
    initial begin
        clk=1;
        reset=0;
        D=4'd1;
        #10 reset =0;
    end



endmodule

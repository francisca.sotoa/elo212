`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:    GRUPO 311
// 
// Create Date: 02.05.2021 23:40:45
// Design Name: 
// Module Name: test_ff_asincronico
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: simulacion testbench de un flip flop asincronico
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_ff_asincronico(    
    );
    logic clk, reset;
    logic [2:0] D, Q;
    
    flipflop_asincronico DUT( 
        .clk(clk),
        .reset(reset),
        .D(D),
        .Q(Q)
    );
    always #5 clk=~clk;
    always #7 reset=~reset;
    initial begin
        clk=0;
        reset=1;
        D=3'd5;
        //#7 reset=0;
    end
endmodule

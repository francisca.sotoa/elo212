`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:    GRUPO 311
// 
// Create Date: 02.05.2021 22:50:49
// Design Name: 
// Module Name: flipflop_asincronico
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: diseño de flip flop flipflop_asincronico que suma 2 y resetea
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module flipflop_asincronico(
    input logic [2:0] D,
    input logic clk,
    input logic reset,
    output logic [2:0]  Q
    );
    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin 
            Q=0;
        end
        else begin
            //Q<=D;
            Q<=D+2;
        end
    end
endmodule

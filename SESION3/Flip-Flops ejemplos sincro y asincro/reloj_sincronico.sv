`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.05.2021 22:53:41
// Design Name: 
// Module Name: reloj_sincronico
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Trabajo previo, punto numero 2, Reloj Sincrónico
module reloj_sincronico(
    input logic clk,reset,
    input logic [3:0] D,
    output logic [3:0] Q //Q es contador
    );
     
    always_ff @(posedge clk) begin
        if(reset) 
            Q<=4'b0;
        else 
            
            Q<=D;
            
     end    
    
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.05.2021 23:35:42
// Design Name: 
// Module Name: divisor_frecuencia
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module divisor_frecuencia#(parameter frec_in = 100000000, parameter frec_out=50000000)
    (input logic clk_in,
    input logic reset,
    output logic clk_out);
    
    localparam COUNTER_MAX= frec_in / (2*frec_out);
    localparam DELAY_WIDTH=$clog2(COUNTER_MAX);
    //localparam frecuencia_salida= frec_in / (2*COUNTER_MAX);
     
    logic [DELAY_WIDTH-1:0] counter = 'd0;
    //Resetea el contador e invierte el valor del reloj de salida
//cada vez que el contador llega a su valor maximo

always_ff @(posedge clk_in) begin
    if(reset == 1'b1) begin 
        //Rest sincronico, setea el contador y la salida a un valor conocido
        counter<= 'd0;
        clk_out <= 0;
    end else if (counter == COUNTER_MAX-1) begin
        //se resetea el contador y se invierte la salida
        counter <= 'd0;
        clk_out <= ~clk_out;
    end else begin
        //se incrementa el contador y se mantiene la salida con su valor anterior
        counter <= counter + 'd1;
        clk_out <= clk_out;
    end    
end

    
    
    
    
    
    
endmodule

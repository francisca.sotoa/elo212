`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2021 22:26:13
// Design Name: 
// Module Name: multiplexor_4entradas
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiplexor_4entradas(
    input logic [6:0] BCD_in_1,BCD_in_2,BCD_in_3,BCD_in_4, //BDC 8421 input 
    [1:0]sel,
    output logic [6:0]out 
    );
    
    //always @ (A,B,C,D,sel) begin //Se eligen los casos para sel y dependiendo de su valor la salida ser� A,B,C o D
    always_comb begin    
        case(sel)
            2'b00 : out=BCD_in_1;
            2'b01 : out=BCD_in_2;      
            2'b10 : out=BCD_in_3;
            2'b11 : out=BCD_in_4;
            
        endcase
    end
endmodule

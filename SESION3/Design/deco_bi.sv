`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.05.2021 23:58:16
// Design Name: 
// Module Name: deco_bi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module deco_bi(
    input logic [1:0] A, 
    output logic [3:0] out
    
    );
    logic [1:0]sel; 
    always_comb begin
        sel=A;    
        case(sel)
            2'b00 : out=4'b0001;
            2'b01 : out=4'b0010;     
            2'b10 : out=4'b0100;
            2'b11 : out=4'b1000;
            default: out=4'b0000;
        endcase
    end
    
    
    
    
    
    
    
endmodule

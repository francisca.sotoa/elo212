`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2021 14:50:15
// Design Name: 
// Module Name: sevenSeg_param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sevenSeg_param #( parameter SIZE_IN=16 )(
    
    input logic [SIZE_IN - 1:0] BCD_in,
    
    output logic [6:0] sevenSeg
    );
    
    
    
    
            always_comb begin          //always_comb begin
                case (BCD_in)
                    4'd0: sevenSeg = ~7'b1111110;//0 fib 0000
                    4'd1: sevenSeg = ~7'b0110000;//1 fib 0001
                    4'd2: sevenSeg = ~7'b1101101;//2 fib 0010
                    4'd3: sevenSeg = ~7'b1111001;//3 no fib 0011
                    4'd4: sevenSeg = ~7'b0110011;//4 fib 0100
                    4'd5: sevenSeg = ~7'b1011011;//5 fib 0101
                    4'd6: sevenSeg = ~7'b1011111;//6 no fib 0110
                    4'd7: sevenSeg = ~7'b1110000;//7 no fib 0111
                    4'd8: sevenSeg = ~7'b1111111;//8 fib 1000
                    4'd9: sevenSeg = ~7'b1110011;//9 fib 1001
                    default: sevenSeg=~7'b0000000;
                endcase
            end
            
                    
    
    
    
    
    
endmodule

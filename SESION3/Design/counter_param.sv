`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2021 21:03:17
// Design Name: 
// Module Name: counter_param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_param#(parameter SIZE=16 )(
    input logic  clk,
    input logic reset,
    output logic [SIZE - 1 :0] count //count
    );
    
    always_ff @(posedge clk) begin
        if (reset)
            count<= 'b0; //count
        else
            count<=count+1;//count
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2021 17:48:32
// Design Name: 
// Module Name: modulos_multiplex_temporal
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module modulos_multiplex_temporal(
    //input logic clk_in,reset,
    input logic clk_in,reset1,reset2,
    output logic [6:0] multiplexor_sevenSeg,
    output logic [3:0] posicion_sevenSeg //out deco
    );
    logic clk_out_1;//clock salida 16 bits 
    logic clk_out_2;//clock salida 2 bits
    logic [15:0] count1;
    logic [1:0] count2;
    logic [1:0] sel;
    
    /*logic [3:0] BCD_in_1;
    logic [3:0] BCD_in_2;
    logic [3:0] BCD_in_3;
    logic [3:0] BCD_in_4;*/
    
    logic [6:0] sevenSeg_1;
    logic [6:0] sevenSeg_2;
    logic [6:0] sevenSeg_3;
    logic [6:0] sevenSeg_4;
    
    
    
    
    
    divisor_frecuencia #(//Divisor contador de 16 bits
            .frec_in(100),//100 MHz
            .frec_out(12.5) // 12,5 MHz 
            
        )div_frec_1(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out_1),
            .reset(reset1)
            
        );
     
     divisor_frecuencia #( //Divisor contador de 2 bits
            .frec_in(100),//100 MHz
            .frec_out(50) // 50 MHz
            
        )div_frec_2(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out_2),
            .reset(reset2)
            
        );
      
     counter_param #(//Contador de 16 bits
            .SIZE(16)
             
            
        )counter_16bit(
            .clk(clk_out_1),//instanciar entradas y salidas
            .count(count1),
            .reset(reset1)
            
        );
        
        
        
        
        counter_param #(//Contador 2 bits
            .SIZE(2)
                  
        )counter_2bit(
            .clk(clk_out_2),//instanciar entradas y salidas
            .count(count2),
            .reset(reset2)
            
        );
        
        /*assign BCD_in_1=count1[3:0];
        assign BCD_in_2=count1[7:4];
        assign BCD_in_3=count1[11:8];
        assign BCD_in_4=count1[15:12];
        assign {>>{BCD_in4, BCD_in3, BCD_in2, BCD_in1}} = contador_16bits;*/
        
        
        //Instancias de SevenSeg con el contador de 16 bits separados en 4 bits cada uno
        sevenSeg_param #(
            .SIZE_IN(4)
                  
        )BCD_1(
            .BCD_in(count1[3:0]),//.BCD_in(BCD_in_1),//instanciar entradas y salidas
            .sevenSeg(sevenSeg_1)
            
            
        );
        
        
        sevenSeg_param #(
            .SIZE_IN(4)
                  
        )BCD_2(
            .BCD_in(count1[7:4]),//.BCD_in(BCD_in_2),//instanciar entradas y salidas
            .sevenSeg(sevenSeg_2)
            
            
        );
        
        
        
        sevenSeg_param #(
            .SIZE_IN(4)
                  
        )BCD_3(
            .BCD_in(count1[11:8]),//.BCD_in(BCD_in_3),//instanciar entradas y salidas
            .sevenSeg(sevenSeg_3)
            
            
        );
        sevenSeg_param #(
            .SIZE_IN(4)
                  
        )BCD_4(
            .BCD_in(count1[15:12]),//instanciar entradas y salidas
            .sevenSeg(sevenSeg_4)
            
            
        );
        
        
        multiplexor_4entradas mux(
            .BCD_in_1(sevenSeg_1),//instanciar entradas y salidas
            .BCD_in_2(sevenSeg_2),
            .BCD_in_3(sevenSeg_3),
            .BCD_in_4(sevenSeg_4),
            .sel(count2),
            .out(multiplexor_sevenSeg));
        
        deco_bi deco(
            .A(count2),//instanciar entradas y salidas
            .out(posicion_sevenSeg)
            
            );
            
           initial begin
            count1='d0;
            count2='d0;
           end

        
        
        
        
        
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.05.2021 01:05:04
// Design Name: 
// Module Name: modulos_divisor_frecuencia
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module modulos_divisor_frecuencia(
     input logic clk_in,reset,
    output logic clk_out_50M,clk_out_30M,clk_out_10M,clk_out_1M

);
    
    
    divisor_frecuencia #(
            .frec_in(100000000),//100 MHz
            .frec_out(50000000) //50 MHz
            
        )mod1(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out_50M),
            .reset(reset)
            
        );
     
     divisor_frecuencia #(
            .frec_in(100000000),//100 MHz
            .frec_out(30000000) //30 MHz
            
        )mod2(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out_30M),
            .reset(reset)
            
        );
      
     divisor_frecuencia #(
            .frec_in(100000000),//100 MHz
            .frec_out(10000000) //10 MHz
            
        )mod3(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out_10M),
            .reset(reset)
            
        );
        
     divisor_frecuencia #(
            .frec_in(100000000),//100 MHz
            .frec_out(1000000) //1 MHz
            
        )mod4(
            .clk_in(clk_in),//instanciar entradas y salidas
            .clk_out(clk_out_1M),
            .reset(reset)
            
        );
endmodule

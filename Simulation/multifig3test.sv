`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2021 00:47:34
// Design Name: 
// Module Name: multifig3test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multifig3test();
    logic [3:0] A,B,C,D;//definicion de conexiones virtuales
    logic [1:0]sel;
    logic [3:0]out;
    mulfig3 DUT( //instancia del modulo a testear
        .A(A),
        .B(B),
        .C(C),
        .D(D),
        .sel(sel),
        .out (out));
    
    initial begin //aca se asignan valores de prueba o estimulos
        A=4'b0000; //0 en binario, 4'b0000 es equivalente a 4'd0
        #3 //retardo de 3 unidades de tiempo basadas en timesacale
    
        B=4'b0001;//1 en binario
        #3
        C=4'b0011;//3 en binario, 4'b0011 es equivalente a 4'd3
        #3
        D=4'b0111;//7 en binario
        #3
        sel=2'b10;
        #3
        sel=2'b00;
        #5
        sel=2'b11;
        #4
        sel=2'b01;
       
        
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2021 18:45:46
// Design Name: 
// Module Name: sum1btest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Ejercicio 3.11 - TestBench para Sumador de 1 bit
module sum1btest();
    logic A,B;
    logic out;
    sum1b DUT( //Dise�o sum1b
        .A(A),
        .B(B),
        .out(out));
    
    initial begin
        A=1'b0;
        B=1'b0;
        #3
        A=1'b0;
        B=1'b1;
        #3
        A=1'b1;
        B=1'b0;
        #3
        A=1'b1;
        B=1'b1;
        
       
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2021 23:23:28
// Design Name: 
// Module Name: sum2bittest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sum2bittest(
    );
    logic [1:0] A, B, C;
    logic [1:0] out;
    
    sum2bit DUT(
        .A(A),
        .B(B),
        .C(C),
        .out(out)
    );
    initial begin
        A=2'b00;
        B=2'b00;
        C=2'b00;
        #3
        A=2'b01;
        B=2'b00;
        C=2'b00;
        #3
        A=2'b01;
        B=2'b01;
        C=2'b00;
        #3
        A=2'b01;
        B=2'b01;
        C=2'b01;
        #3
        A=2'b11;
        B=2'b01;
        C=2'b01;
        #3
        A=2'b11;
        B=2'b11;
        C=2'b01;
        #3
        A=2'b11;
        B=2'b11;
        C=2'b11;
        
    end
endmodule

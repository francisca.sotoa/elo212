`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2021 01:26:04
// Design Name: 
// Module Name: multifig4test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multifig4test();
    logic [3:0] A,B,C,D,E;//definicion de conexiones virtuales
    logic [2:0]sel;
    logic [3:0]out;
    multifig4 DUT( //instancia del modulo a testear
        .A(A),
        .B(B),
        .C(C),
        .D(D),
        .E(E),
        .sel(sel),
        .out (out));
    
    initial begin //aca se asignan valores de prueba o estimulos
         /*
          Posibles valores de sel
          000 : out=A; //0
          001 : out=B; //1     
          010 : out=C; //2
          011 : out=D;  //3
          100 : out=E;*/ //4
        A=4'b0000; //0 en binario, 4'b0000 es equivalente a 4'd0
        #3 //retardo de 3 unidades de tiempo basadas en timesacale
    
        B=4'b0001;//1 en binario
        #3
        C=4'b0011;//3 en binario, 4'b0011 es equivalente a 4'd3
        #3
        D=4'b0111;//7 en binario
        #3
        E=4'b0101;//5
        #3
        sel=3'd1;//1
        #2
        sel=3'd4;//4
        #2
        sel=3'd0;//0
        #2
        sel=3'd3;//3
        #2
        sel=3'd3;//3
        #8
        sel=3'd7;//7
        
        
        
       
        
    end
endmodule

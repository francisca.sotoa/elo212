`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// Create Date: 20.04.2021 22:08:54
// Design Name: Fibbinario
// Module Name: testfibb
// Project Name: sesion 1 elo212

// Description: modo de prueba para detectar numeros fibbinario 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//archivo simulacion para probar el fibbinario
module testfibb();
    logic A, B, C, D; //creacion de entradas
    logic S;  //creacion salida
    
    fibbinario DUT( //asignacion de prueba
    .A (A),
    .B (B),
    .C (C),
    .D (D),
    .S (S)
    );
    initial begin  //comienza la prueba
    A=1'b1; //asignamos valores a cada bit
    B=1'b0;
    C=1'b0;
    D=1'b1;
    #3
    A=1'b1;
    B=1'b1;
    C=1'b0;
    D=1'b1;
 /*   #3
    A=1'b1;
    B=1'b0;
    C=1'b1;
    D=1'b0;
   */   
    end  //fin prueba
endmodule

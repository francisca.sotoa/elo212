`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2021 16:45:06
// Design Name: 
// Module Name: fulladd1btest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fulladd1btest();
    logic  A,B,Cin;//definicion de conexiones virtuales
    logic S,Cout;
    fulladd1b DUT(
        .A(A),
        .B(B),
        .Cin(Cin),
        .S(S),
        .Cout(Cout));
        
    initial begin
        A=1'b0;
        B=1'b0;
        Cin=1'b0;
        #3
        A=1'b0;
        B=1'b0;
        Cin=1'b1;
        #3
        A=1'b0;
        B=1'b1;
        Cin=1'b0;
        #3
        A=1'b0;
        B=1'b1;
        Cin=1'b1;
        #3
        A=1'b1;
        B=1'b0;
        Cin=1'b0;
        #3
        A=1'b1;
        B=1'b0;
        Cin=1'b1;
        #3
        A=1'b1;
        B=1'b1;
        Cin=1'b0;
        #3
        A=1'b1;
        B=1'b1;
        Cin=1'b1;
    
    end
    
    
    
endmodule

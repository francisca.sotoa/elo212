`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2021 17:21:48
// Design Name: 
// Module Name: fulladder2bittest
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fulladder2bittest();
    logic  A0,A1,B0,B1,Cin;//definicion de conexiones virtuales
    logic S0,S1,Cout;
    fulladder2b DUT(
        .A0(A0),
        .A1(A1),
        .B0(B0),
        .B1(B1),     
        .Cin(Cin),
        .S0(S0),
        .S1(S1),
        .Cout(Cout));
        
    initial begin
        A0=1'b0;
        B0=1'b0;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b0; ////////////////
        #3
        A0=1'b0;
        B0=1'b0;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b1;//////////////
        #3
        A0=1'b0;
        B0=1'b1;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b0; /////////////
        #3
        A0=1'b0;
        B0=1'b1;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b1; /////////////
        #3
        A0=1'b0;
        B0=1'b0;
        A1=1'b0;
        B1=1'b1;
        Cin=1'b0; //////////////
        #3
        A0=1'b0;
        B0=1'b0;
        A1=1'b0;
        B1=1'b1;
        Cin=1'b1;
        #3
        A0=1'b0;
        B0=1'b1;
        A1=1'b0;
        B1=1'b1;
        Cin=1'b0;
        #3
        A0=1'b0;
        B0=1'b1;
        A1=1'b0;
        B1=1'b1;
        Cin=1'b1;
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b0;
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b1;
        #3
        A0=1'b1;
        B0=1'b1;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b0; //fila 11 01010
        #3
        A0=1'b1;
        B0=1'b1;
        A1=1'b0;
        B1=1'b0;
        Cin=1'b1;
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b0;
        B1=1'b1;
        Cin=1'b0;
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b0;
        B1=1'b1;
        Cin=1'b1; //fila 14
        
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b0;
        B0=1'b0;
        Cin=1'b0;//10000
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b0;
        B0=1'b0;
        Cin=1'b1;//10001
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b0;
        B0=1'b1;
        Cin=1'b0;//10010
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b0;
        B0=1'b1;
        Cin=1'b1;//10011
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b1;
        B0=1'b0;
        Cin=1'b0;//10100
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b1;
        B0=1'b0;
        Cin=1'b1;//10101
         #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b1;
        B0=1'b1;
        Cin=1'b0;//10110
        #3
        A1=1'b1; 
        A0=1'b0;
        B1=1'b1;
        B0=1'b1;
        Cin=1'b1;//10111
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b0;
        B0=1'b0;
        Cin=1'b0;//11000
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b0;
        B0=1'b0;
        Cin=1'b1;//11001
         #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b0;
        B0=1'b1;
        Cin=1'b0;//11010
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b0;
        B0=1'b1;
        Cin=1'b1;//11011
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b0;
        Cin=1'b0;//11100
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b0;
        Cin=1'b1;//11101
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b1;
        Cin=1'b0;//11110
        #3
        A1=1'b1; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b1;
        Cin=1'b1; //11111
        
        #3
        A1=1'b0; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b1;
        Cin=1'b1; //01111
        #3
        A1=1'b0; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b1;
        Cin=1'b0; //01110
        #3
        A1=1'b0; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b0;
        Cin=1'b1; //01101
        #3
        A1=1'b0; 
        A0=1'b1;
        B1=1'b1;
        B0=1'b0;
        Cin=1'b0; //01100
        #3
        A1=1'b0; 
        A0=1'b1;
        B1=1'b0;
        B0=1'b1;
        Cin=1'b1; //01011
     /*       
  //de abajo pa arriba      
        #3
        A0=1'b1;
        B0=1'b1;
        A1=1'b1;
        B1=1'b1;
        Cin=1'b0;
        #3
        A0=1'b1;
        B0=1'b1;
        A1=1'b1;
        B1=1'b1;
        Cin=1'b1;
        
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b1;
        B1=1'b1;
        Cin=1'b0;
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b1;
        B1=1'b1;
        Cin=1'b1;
        
        #3
        A0=1'b1;
        B0=1'b1;
        A1=1'b1;
        B1=1'b0;
        Cin=1'b0;
        #3
        A0=1'b1;
        B0=1'b1;
        A1=1'b1;
        B1=1'b0;
        Cin=1'b1;
        
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b1;
        B1=1'b0;
        Cin=1'b0;
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b1;
        B1=1'b0;
        Cin=1'b1;
        
        #3
        A0=1'b1;
        B0=1'b0;
        A1=1'b1;
        B1=1'b0;
        Cin=1'b0;  */
    
    end
endmodule

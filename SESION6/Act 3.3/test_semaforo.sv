`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.06.2021 01:32:53
// Design Name: 
// Module Name: test_semaforo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_semaforo(
    );
    logic clk, reset, TA, TB;
    logic [2:0] LA, LB;
    semaforo DUT(
        .clk(clk),
        .reset(reset),
        .TA(TA),
        .TB(TB),
        .LA(LA),
        .LB(LB)
    );
    always #5 clk=~clk;
    initial begin
    clk=0;
    reset=0;
    #6//6
    reset=1;
    TA=0;
    TB=0;
    
    #4 //10
    reset=0;
    
    #5 //15
    TA=1;
    
    #4  //19
    TB=1;
    
    #3  //22
    TA=0;
    
    #5  //27
    TB=0;
    
    #4   //31
    TA=1;
    #10  //41
    TA=0;
    TB=1;
    
    #12  //53
    TA=0;
    TB=0;
    
    #5   //58
    TA=1;
    TB=0;
    
    #10  //68
    TB=1;
    
    end
    
endmodule


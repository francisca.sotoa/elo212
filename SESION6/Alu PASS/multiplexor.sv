`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 04:38:22
// Design Name: 
// Module Name: multiplexor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiplexor(
    input logic [6:0] BCD_in_1,BCD_in_2,BCD_in_3,BCD_in_4,//BCD_in_5,BCD_in_6,BCD_in_7,BCD_in_8, //BDC 8421 input 
    input logic [2:0]sel,//selector de 3 bits porque hay 8 opciones de bcd-display
    output logic [6:0]segments //salida definitiva al display
    );
    
     //Se eligen los casos para sel y dependiendo de su valor la salida ser� BCD_in_x
    always_comb begin    
        case(sel)
            3'b000 : segments=BCD_in_1;
            3'b001 : segments=BCD_in_2;      
            3'b010 : segments=BCD_in_3;
            3'b011 : segments=BCD_in_4;
            /*3'b100 : segments=BCD_in_5;
            3'b101 : segments=BCD_in_6;      
            3'b110 : segments=BCD_in_7;
            3'b111 : segments=BCD_in_8;*/
            default: segments=3'd0;//se cubren todos los casos
        endcase
    end
    
    
endmodule
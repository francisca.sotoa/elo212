`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.06.2021 17:11:31
// Design Name: 
// Module Name: reg_nuevo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


// CON LA LOGICA DENTRO DEL ALWAYS_FF
module reg_nuevo#(parameter N=16)(
    input logic [N-1:0] ShiftIn, input logic clk,
    input logic reset, input logic load, output logic [N-1:0] ShiftOut
    );
    
    always_ff @(posedge clk) begin
        if(reset) begin
            ShiftOut<= 'b0;
        end else if (load) begin 
            ShiftOut<=ShiftIn;   
        end else begin
            ShiftOut<=ShiftOut;
        end        
  
        /*
        if( Clk) begin
            out=temp_out;
        end else begin
            out=out;
        end */
    end
endmodule

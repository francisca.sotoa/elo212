`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.06.2021 21:31:33
// Design Name: 
// Module Name: test_debouncer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_debouncer(
    );
    logic clk;                  // base clock
	logic rst;                  // global reset
	logic PB;                   // raw asynchronous input from mechanical PB   
	
	//salidas      
	logic PB_pressed_status;    // clean and synchronized pulse for button presse
	logic PB_pressed_pulse;    // high if button is pressed
	logic PB_released_pulse; 
/*	logic PB_pressed_status_FSM;    // clean and synchronized pulse for button presse
	logic PB_pressed_pulse_FSM;    // high if button is pressed
	logic PB_released_pulse_FSM;
	
	top_module_debouncer DUT(
        .clk(clk),
        .rst(rts),
        .PB(PB),
        .PB_pressed_status(PB_pressed_status),
        .PB_pressed_pulse(PB_pressed_pulse),
        .PB_released_pulse(PB_released_pulse),
        .PB_pressed_status_FSM(PB_pressed_status_FSM),
        .PB_pressed_pulse_FSM(PB_pressed_pulse_FSM),
        .PB_released_pulse_FSM(PB_released_pulse_FSM)
    ); */
    debouncer_counter #(.DELAY(15)) DUT(
        .clk(clk),
        .rst(rst),
        .PB(PB),
        .PB_pressed_status(PB_pressed_status),
        .PB_pressed_pulse(PB_pressed_pulse),
        .PB_released_pulse(PB_released_pulse)
    );
 /*   
    debouncer_FSM #(.N(15)) DUT(
        .clk(clk),
        .rst(rst),
        .PB(PB),
        .PB_pressed_status(PB_pressed_status),
        .PB_pressed_pulse(PB_pressed_pulse),
        .PB_released_pulse(PB_released_pulse)
    ); */
    
    
    always #5 clk=~clk;
    initial begin
    clk=0;
    rst=0;
    #11
    rst=1;
    #2 PB=1;
    #6
    rst=0;
    #10
    PB=0;
    
    #14    
    PB=1;
    
    #15
    PB=0;
    #4
    PB=1;
    #5
    PB=0;
    
	end
endmodule

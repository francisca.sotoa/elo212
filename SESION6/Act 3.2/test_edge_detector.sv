`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.06.2021 00:32:15
// Design Name: 
// Module Name: test_edge_detector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_edge_detector(
    );
    logic clk, reset, L, P;
    edge_detector DUT(
        .clk(clk),
        .reset(reset),
        .L(L),
        .P(P)
    );
    
    always #5 clk=~clk;
    
    initial begin
    clk=0;
    reset=0;
    L=0;
    
    #5
    reset=1;
    
    #5
    reset=0;
    L=1;
    
    #5
    L=0;
    
    #10
    L=1;
    #10
    L=0;
    
    #5
    L=1;
    
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.06.2021 19:24:04
// Design Name: 
// Module Name: edge_detector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module edge_detector(
    input logic clk, reset, L,
    output logic P
    );
    
    //codificacion de estado
    enum logic [2:0] {s0, s1, s2} state, next_state;
    
    //bloque de memoria FLIPFLOP
    always_ff @(posedge clk) begin  //bloque del clock
        if(reset)
            state<=s0;
        else
            state<= next_state;
    end
    
    always_comb begin
        case(state)
            s0: begin
                if(L)
                    next_state=s1;
               else
                    next_state=s0;      
            end
            s1: begin
                if(L)
                    next_state=s2;
                else
                    next_state=s0;
            end
            s2: begin
                if(L)
                    next_state=s2;
                else
                    next_state=s0;
            end
            default: next_state=s0; //reseteo en caso de falla
           
        endcase
        
    end
    
    //logica de salida output logic
    always_comb begin
        case(state)
            s0: P=0;
            s1: P=1;
            s2: P=0;
            default: P=0;
        endcase
    end
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// Create Date: 17.04.2021 21:26:09
// Design Name: 
// Module Name: cable
// Project Name: Sesion 1 elo212
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module cable(
    input logic A, //declaracion entrada
    output logic B //declaracion salida
    );
    
    assign B = A; //asignacion como cable
endmodule

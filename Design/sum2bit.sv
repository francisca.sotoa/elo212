`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2021 23:17:19
// Design Name: 
// Module Name: sum2bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sum2bit(
        input logic [1:0] A, B, C,
        output logic [1:0] out
    );
    always_comb begin
        out= A+B+C;
    end
endmodule

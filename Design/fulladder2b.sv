`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2021 16:59:47
// Design Name: 
// Module Name: fulladder2b
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fulladder2b(
    input logic A1,A0,B1,B0,Cin,
    output logic Cout,S0, S1
    );
    logic temp0;
    logic temp1;
    logic temp2;
    logic temp3;
    logic Carry;   
    assign temp0=( ~A0  | ~B0  )&( (A0|B0) & (A0|B0)  );
    assign temp1= temp0 & Cin;
    assign Carry= ((A0&B0) | temp1);
    assign S0=(  ~temp0|~Cin )&( (temp0|Cin)& (temp0|Cin) );

    assign temp2=( ~A1  | ~B1  )&( (A1|B1) & (A1|B1)  );
    assign temp3= temp2 & Carry;
    assign Cout= ((A1&B1) | temp3);
    assign S1=(  ~temp2|~Carry )&( (temp2|Carry)& (temp2|Carry) );


endmodule

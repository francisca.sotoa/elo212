`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// 
// Create Date: 20.04.2021 21:39:21
// Design Name: 
// Module Name: fibbinario
// Project Name: Sesion 1 ELO212

// Description: dise�o para detectar numeros fibbinarios

// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: grupo 311
// 
//////////////////////////////////////////////////////////////////////////////////


module fibbinario(
    input logic A, B, C, D, //declaracion entradas
    output logic S //declaracion unica salida
    );
    logic temp, temp1, temp2; //declaro temporales para cada relacion
    logic Ac, Bc, Cc, Dc;
    assign Ac=~A;
    assign Bc=~B;
    assign Cc=~C;
    assign Dc=~D;
    assign temp= Ac & Cc; //asignacion declaracion de temporales
    assign temp1= Bc & Cc;
    assign temp2= Bc & Dc;
    assign S= temp | temp1 | temp2; //asigno el valor de la salida
endmodule

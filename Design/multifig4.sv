`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2021 01:13:49
// Design Name: 
// Module Name: multifig4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//3.9
//Multiplexor figura 5

module multifig4(
    input logic [3:0] A,B,C,D,E, //BDC 8421 input 
    [2:0]sel,
    output logic [3:0]out 
    );
    
    //always @ (A,B,C,D,E,sel) begin
    always_comb begin
        case(sel)
            3'd0 : out=A; //000
            3'd1 : out=B; //001     
            3'd2 : out=C; //010
            3'd3 : out=D; //011
            3'd4 : out=E; //100
            default out=0; //111,etc.
        endcase
    end
 endmodule

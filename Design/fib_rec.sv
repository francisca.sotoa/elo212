`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.04.2021 22:46:48
// Design Name: 
// Module Name: fib_rec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fib_rec(
    input logic [3:0] BCD_in, //BCD 8421 input
    output logic fib  //high if input is fibbinary
    );
    always_comb begin       //compara num fibbinarios en decimal. So fibb: 0-1-2-4-5-8-9-10  se pueden escribir en 4 bits
        if(BCD_in==4'd0 || BCD_in==4'd1 || BCD_in==4'd2 || BCD_in==4'd4 ||
            BCD_in==4'd5 || BCD_in==4'd8 || BCD_in==4'd9 || BCD_in==4'd10)
                fib=1;
        else
                fib=0;
     end
endmodule

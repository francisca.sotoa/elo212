`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2021 16:17:00
// Design Name: 
// Module Name: fulladd1b
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fulladd1b(
    input logic A,B,Cin,
    output logic Cout,S
    );
    logic temp0;
    logic temp1;
       
    assign temp0=( ~A  | ~B  )&( (A|B) & (A|B)  );
    assign temp1= temp0 & Cin;
    assign Cout= ((A&B) | temp1);
    assign S=(  ~temp0|~Cin )&( (temp0|Cin)& (temp0|Cin) );
       
endmodule

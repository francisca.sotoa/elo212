`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2021 00:13:03
// Design Name: 
// Module Name: mulfig3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//3.9
//Multiplexor figura 4
module mulfig3(
    input logic [3:0] A,B,C,D, //BDC 8421 input 
    [1:0]sel,
    output logic [3:0]out 
    );
    
    always_comb begin //Se eligen los casos para sel y dependiendo de su valor la salida ser A,B,C o D
        case(sel)
            2'b00 : out=A;
            2'b01 : out=B;      
            2'b10 : out=C;
            2'b11 : out=D;
        endcase
    end
 endmodule

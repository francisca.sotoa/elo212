`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2021 18:28:24
// Design Name: 
// Module Name: sum1b
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Ejercicio 3.11 - Sumador de 1 bit
module sum1b(
    input logic A,B,
    output logic out
    );
    //logic temp; en el caso que se necesite una variable temporal
    //Hacemos la suma de numeros de 1 bit
    
    always_comb begin
        out=A+B;
        
    end
endmodule

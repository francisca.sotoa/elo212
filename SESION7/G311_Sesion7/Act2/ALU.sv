`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.06.2021 23:06:13
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU #(parameter N=16)(
    input  logic [N-1:0]   A, B,
	input  logic [1:0]   OpCode,
	output logic [N-1:0]   Result,
	output logic [3:0]   Status  //{N,Z,C,V}

    );
    logic [N:0] flag;
    always_comb begin
        case(OpCode)
            2'b00: begin
                        flag=A+B;
                        Result=flag[N-1:0];
                        if(flag[N]==1'b1)begin //Para revisar si hay carry out
                            Status[1]=1'b1;//C=1
                        end
                        else begin
                            Status[1]=1'b0;//C=0
                        end
                        
                        //Suma de A+B Sistema con signo
                        if(((A[N-1]& B[N-1]& ~Result[N-1])| (~A[N-1]& ~B[N-1]& Result[N-1]))==1  )begin
                            Status[0]=1'b1;// Para revisar si hay overflow, V=1
                        end
                        else begin
                            Status[0]=1'b0;//V=0
                        end
             
                   end
            2'b01: begin
                        flag=A-B;
                        Result=flag[N-1:0];
                        //Resta de A-B Sistema sin signo
                        if(flag[N]==1'b1)begin
                            Status[1]=1'b1;//C=1
                        end
                        else begin
                            Status[1]=1'b0;//C=0
                        end
                        
                        //Resta de A-B Sistema con signo
                        if(((~A[N-1]& B[N-1]& Result[N-1])|(A[N-1]& ~B[N-1]& ~Result[N-1]))==1 )begin
                            Status[0]=1'b1;
                        end
                        else begin
                            Status[0]=1'b0;
                        end
             
                   end
            2'b10: begin
                        Result= A|B;
                        Status[1:0]=2'b00;
                   end 
            
            2'b11: begin
                        Result= A&B;
                        Status[1:0]=2'b00;
                        
                        
                   end  
        endcase
        
        if(Result=='b0)begin //Si el resultado de la operacion es 0 , Z=1
            Status[2]=1'b1;
        end
        else begin
            Status[2]=1'b0;
        end
        
        if(Result[N-1]==1'b1)begin //Asumimos que el bit mas significativo es 1 para negativo y 0 para positivo 
            Status[3]=1'b1;
        end
        else begin
            Status[3]=1'b0;//Si es positivo se setea N=0
        end
        

    
    
    
    end
    
endmodule

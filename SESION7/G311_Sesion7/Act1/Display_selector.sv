`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.06.2021 20:28:57
// Design Name: 
// Module Name: Display_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Display_selector#(parameter N=16)(
    input logic clk,
    input logic reset, 
    input logic [N-1:0] Result,
    input logic [N-1:0] DataIn,
    input logic ToDisplaySel,
    output logic [N-1:0] ToDisplay
    
    );
    
    always_ff @(posedge clk) begin
        if(~reset) begin
           ToDisplay<='b0;
        end else begin
            if(ToDisplaySel) begin //Decision de dise�o, si el ToDisplaySel es 1 se guarda el Result, si no se guarda el DataIn
                ToDisplay<=Result;
            end else begin
                ToDisplay<=DataIn;
            end
        end
    end
endmodule


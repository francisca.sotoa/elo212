`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.06.2021 22:27:53
// Design Name: 
// Module Name: ReversePolishFSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ReversePolishFSM(
    input logic Enter_pulse, //entrada ya filtrada
    input logic reset,
    input logic clk,
    output logic [2:0]Status,//De cuantos bits el Status? 3 o 6 bits??
    output logic updateRes,
    output logic ToDisplaySel,
    output logic LoadOpA,
    output logic LoadOpB,
    output logic LoadOpCode  
    );
    

    
    enum logic [6:0] {Entering_OpA, Load_OpA, Entering_OpB, Load_OpB, Entering_OpCode, Load_OpCode, Show_Result} state, next_state;
    
    always_comb begin
        Status= 1'd0;
        LoadOpA= 1'b0;
        LoadOpB= 1'b0;
        LoadOpCode=1'b0;
        ToDisplaySel=1'b0;
        updateRes=1'b0;
        next_state = Entering_OpA;
        
        
        case(state)
            Entering_OpA: begin
                            //agregar las condiciones para los cambios
                            if (Enter_pulse) begin
                                next_state=Load_OpA;
                            end else begin
                                next_state=Entering_OpA;
                            end
                                                        
                          end
                          
            Load_OpA: begin
                        Status=3'd1;
                        LoadOpA=1'd1;
                        next_state=Entering_OpB;
                      end
                      
            Entering_OpB: begin
                            Status= 3'd2;
                            if(Enter_pulse) begin
                                next_state=Load_OpB;
                            end else begin
                                next_state=Entering_OpB;
                            end
                          end
                          
            Load_OpB: begin
                        Status=3'd3;
                        LoadOpB='b1;
                        next_state=Entering_OpCode;
                      end
                      
            Entering_OpCode: begin
                                Status= 3'd4;
                                if(Enter_pulse) begin
                                    next_state=Load_OpCode;
                                end else begin
                                    next_state=Entering_OpCode;
                                end
                             end
                             
            Load_OpCode: begin
                            Status=3'd5;
                            LoadOpCode='b1;
                            next_state=Show_Result;
                         end
                         
            Show_Result: begin
                            Status=3'd6;
                            ToDisplaySel='b1;//Decision de dise�o, si el estado es Show_Result, entonces
                                            //el ToDisplaySel es 1 y se mostrara el Resultado.
                            updateRes='b1;
                            if (Enter_pulse) begin
                                next_state=Entering_OpA;
                            end else begin
                                next_state=Show_Result;
                            end
                         end
      
        endcase
        
    end
    
    //actualizacion
    always_ff @(posedge clk) begin
        if(~reset) 
            state <= Entering_OpA;
        else 
            state <= next_state;
    end
    
    
endmodule


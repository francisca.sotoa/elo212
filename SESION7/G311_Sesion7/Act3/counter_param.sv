`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.06.2021 00:13:13
// Design Name: 
// Module Name: counter_param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_param#(parameter SIZE=3 )(//contador de 3 bits para obtener sel del MUX y entrada del decodificador binario
    input logic  clk,
    input logic reset,
    output logic [SIZE - 1 :0] count 
    );
    
    always_ff @(posedge clk) begin
        if (~reset)
            count<= 'b0; 
        else
            count<=count+1;
    end

endmodule

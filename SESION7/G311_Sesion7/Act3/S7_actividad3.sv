`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.06.2021 23:57:40
// Design Name: 
// Module Name: S7_actividad3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S7_actividad3 #(parameter N_DEBOUNCER=10)(//ANTES ERA 10
    input logic clk,
    input logic resetN,
    input logic Enter,Undo,DisplayFormat,
    input logic  [15:0] DataIn,
    
    output logic [6:0] Segments,
    output logic [7:0] Anodes,
    output logic [3:0] Flags, //{N,Z,C,V}
    output logic [2:0] Status //Indica de manera secuencial el estado en el que se encuentra
    );
    
   logic [31:0] ToDisplayInterno;
    
    //salida del unsigned_to_bcd
    logic [31:0] bcd_salida_dd;
    //logic idle; //lo conecto a tierra
    //saluda del mux-entrada al 7seg
    logic [31:0] bcd_out;
    
    act2 #(.N_DEBOUNCER(N_DEBOUNCER)) moduloAct2 (
        .clk(clk),
        .resetN(resetN),
        .Enter(Enter),
        .Undo(Undo),
        .DataIn(DataIn),
        .ToDisplay(ToDisplayInterno),
        .Flags(Flags),
        .Status(Status)
    );
    
    unsigned_to_bcd DoubleDabble(
        .clk(clk),
        .reset(resetN),//cambiar reset
        .trigger(1'b1),
        .in(ToDisplayInterno),
        //.idle(idle), //A tierra, no se usa
        .bcd(bcd_salida_dd)//Salida del Unsigned
    );
    
    Mux_display32bits MUX_sevenSeg(
        .ToDisplay(ToDisplayInterno),
        .bcd(bcd_salida_dd),
        .displayFormat(DisplayFormat),
        .BCD_out(bcd_out)
    );
    
    Hex_to_sevenSeg to_sevenSeg(
        .BCD_in(bcd_out),
        .clock(clk),
        .reset(resetN),
        .segments(Segments),
        .anodos(Anodes)
    );
    
endmodule

        
        
    
    

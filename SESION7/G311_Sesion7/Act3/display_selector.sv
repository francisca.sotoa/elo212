`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.06.2021 00:10:22
// Design Name: 
// Module Name: Display_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Display_selector#(parameter N=16)(
    input logic clk,
    input logic reset, 
    input logic [N-1:0] Result,
    input logic [N-1:0] DataIn,
    input logic ToDisplaySel,
    output logic [31:0] ToDisplay //[2*N-1:0] ToDisplay
    
    );
    logic [15:0] B;
    assign B='d0;
    //Decision de dise�o de que la salida ToDisplay sea de 32 bits, se les agrega las cifras mas significativas
    //16 0s para completar el numero
    
    always_ff @(posedge clk) begin
        if(~reset) begin
           ToDisplay<='b0;
        end else begin
            if(ToDisplaySel) begin
                ToDisplay<={B,Result};
            end else begin
                ToDisplay<={B,DataIn};
            end
        end
    end
endmodule
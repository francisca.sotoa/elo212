`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.06.2021 00:37:19
// Design Name: 
// Module Name: act2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module act2 #(parameter N_DEBOUNCER=10)(//ANTES ERA 10
    input logic clk,
    input logic resetN,
    input logic Enter,Undo,
    input logic  [15:0] DataIn,
    output logic [31:0] ToDisplay,//valor de salida para el Display
    output logic [3:0] Flags, //{N,Z,C,V}
    output logic [2:0] Status //Indica de manera secuencial el estado en el que se encuentra
    );
    
     logic pressed_enter_enter;
     logic pressed_enter_undo;
    //logic pressed_status;//Se�al a tierra
    //logic released_pulsed;//Se�al a tierra
    
    //Se�ales Internas Calculadora Polaca
    logic updateR;
    logic todisplaysel;
    logic loadA;
    logic loadB;
    logic loadCode;
    
    //Se�al Interna Entre Result y DisplaySelector
    logic [15:0]resultDisplay;
    
    debouncer_FSM  #(.DELAY(N_DEBOUNCER)) debFSM_ENTER(
        .clk(clk),
        .rst(resetN),
        .PB(Enter),
        .PB_pressed_pulse(pressed_enter_enter)
        
    
    );
    
    debouncer_FSM  #(.DELAY(N_DEBOUNCER)) debFSM_UNDO(
        .clk(clk),
        .rst(resetN),
        .PB(Undo),
        .PB_pressed_pulse(pressed_enter_undo)
        
    
    );
    
    ReversePolishFSM ReversePolish(
        .Enter_pulse(pressed_enter_enter),
        .Undo_pulse(pressed_enter_undo),
        .reset(resetN),
        .clk(clk),
        .Status(Status),
        .updateRes(updateR),
        .ToDisplaySel(todisplaysel),
        .LoadOpA(loadA),
        .LoadOpB(loadB),
        .LoadOpCode(loadCode) 
    
    );
    
    
    
    
    ALU_alto_nivel #(.N(16))alu_alto_nivel(
            .data_in(DataIn),
            .load_A(loadA),
            .load_B(loadB),
            .load_Op(loadCode),
            .updateRes(updateR),
            .clk(clk),
            .reset(resetN),
            .Flags(Flags),
            .Result(resultDisplay)
        
        
        );
        
     Display_selector DisplaySel(
        .clk(clk),
        .reset(resetN),
        .Result(resultDisplay),
        .DataIn(DataIn),
        .ToDisplaySel(todisplaysel),
        .ToDisplay(ToDisplay)
        
     
     
     );
     
    
    
    
    
    
    
    
    
endmodule

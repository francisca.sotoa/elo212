`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.06.2021 00:07:48
// Design Name: 
// Module Name: ALU_alto_nivel
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_alto_nivel#(parameter N=16 ) (
	input  logic [N-1:0]  data_in,
	input  logic load_A , load_B , load_Op , updateRes,
	input logic  clk,reset,
	output logic [3:0] Flags,
	output logic [N-1:0] Result
	//output logic [6:0]   Segments,
	//output logic [7:0]    Anodes,
	//output logic [3:0] LEDs //verificar el tamao del bus de LEDs  

);

//NOTAR QUE DEBE PARAMETRIZAR LA(S) ENTRADA(S)

    //Seales internas para hacer la conexion entre modulos
    
        
    //logic [N-1:0]   A, B;
	//logic [1:0]   Op;
	
	logic [N-1:0] Result1;//Salida de ALU
	
    logic [3:0]   Flags1;  //{N,Z,C,V}
    
    logic [N-1:0] outA;//Salida reg_A y entrada ALU
    logic [N-1:0] outB;//Salida reg_B y entrada ALU 
    logic [1:0] outOp;//Salida reg_Op y entrada ALU
    
    logic [N-1:0] BCD; //parametrizable ?????    
        
        
        reg_nuevo reg_A(
            .clk(clk),
            .ShiftIn(data_in),
            .reset(reset),
            .ShiftOut(outA),
            .load(load_A)//implementar load en reg2
        );
        reg_nuevo reg_B(
            .clk(clk),
            .ShiftIn(data_in),
            .reset(reset),
            .ShiftOut(outB),
            .load(load_B)
        );
        
        reg_nuevo #(.N(2)) reg_Op(
            .clk(clk),
            .ShiftIn(data_in[1:0]),
            .reset(reset),
            .ShiftOut(outOp),
            .load(load_Op)
        );
        
        
        
        
        ALU #(.N(16))alu_central(
            .A(outA),
            .B(outB),
            .OpCode(outOp),
            .Result(Result1),
            .Status(Flags1)
        
        
        );
        
        
        
        
        
        reg_nuevo #(.N(16))reg_Result(
            .clk(clk),
            .ShiftIn(Result1),
            .reset(reset),
            .ShiftOut(Result),//BCD parametrizable ???
            .load(updateRes)
        );
        
        reg_nuevo #(.N(4))reg_Flags(
            .clk(clk),
            .ShiftIn(Flags1),
            .reset(reset),
            .ShiftOut(Flags),
            .load(updateRes)
        );    
        
        
        
        
        
        /*Hex_to_sevenSeg#(.N(16)) HtSg(
            .BCD_in(BCD),//BCD parametrizable ??
            .clock(clk),
            .reset(reset),
            .segments(Segments),
            .anodos(Anodes)
        
        
        );*/   



    
endmodule
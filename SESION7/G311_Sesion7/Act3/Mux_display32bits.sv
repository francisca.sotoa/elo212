`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.06.2021 23:36:23
// Design Name: 
// Module Name: Mux_display32bits
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mux_display32bits(
    input logic [31:0] ToDisplay,
    input logic [31:0] bcd,
    input logic displayFormat,
    output logic  [31:0] BCD_out
    );
    
    always_comb begin      
        case (displayFormat)
            1'b0: BCD_out=ToDisplay; //EN HEXADECIMAL
            1'b1: BCD_out=bcd; //DECIMAL SIN SIGNO
        endcase
    end
endmodule


README

Grupo: 311

Integrantes:
Francisca Soto
Johanny Espinoza
Francisco López

En este README se indican las decisiones de diseño tomadas por el equipo.
----------------------------------------------------------------------------------------
Actividad 1:

Para el estado Show_Result tomamos la decision de diseño de que
en el "Display_Selector" dependiendo de los valores del selector "ToDisplaySel" 
se mostraran distintas entradas:
ToDisplaySel=0->Se muestra el "DataIn"
ToDisplaySel=1->Se muestra el "Result"

Se eliminaron las salidas del "debouncer_FSM" llamadas 
Pb_pressed:status y PB_released_pulse y solo se utilizo una salida
llamada PB_pressed_pulse. 


--------------------------------------------------------------------------------------
Actividad 2:

El "Enter" tiene prioridad sobre "Undo" en la calculadora polaca.

Se toma la misma decision de la actividad 1 en cuanto al "Display_Selector" y "debouncer_FSM".

Debido a que en la guia se especifica que, cuando la maquina esta en estado Entering_OpA, 
solo se moverá al estado Enterin_OpB, por lo que el estado Entering_OpA solo reaccionará
a la señal Enter. Debido a que no hay claridad en este punto se comentaron las lineas 
57-61 del modulo "ReversePolishFSM", en el caso de que Entering_OpA, si reaccione al
pulso Undo.

----------------------------------------------------------------------------------------
Actividad 3:

Se toma la misma decision de la actividad 2 en cuanto al "Display_Selector" y "debouncer_FSM", "Enter" y "Undo".

Se cambia el modulo "Display Selector" y se le agregan los 0s a la izquierda a 
la salida de este, con una salida de 32 bits.

Se creo un multiplexor con logica combinacional llamado "Mux_display32bits" 
para decidir cual sera la salida que ingresara al "Hex_to_sevenSeg":
DisplayFormat=0 -> Se muestra el "ToDisplay"
DisplayFormat=1 -> Se muestra el bcd que viene del double dabble.

En el modulo "unsigned_to_bcd" se comenta la salida idle, pero luego
al instanciar este modulo en el modulo principal "S7_actividad3" tambien se comenta 
la salida "idle".Como el idle no tiene utilidad mas que ver la salida como interna
para ver si hay una conversion, lo dejamos comentado, ya que no afectaria en el
resultado global de nuestro modulo top. 






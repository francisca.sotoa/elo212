`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.04.2021 20:50:23
// Design Name: 
// Module Name: test_top_module_sesion
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_top_module_sesion(
    
    );
    logic clk, reset; //para contador
//    logic [3:0] count;  
//    logic [3:0] BCD_in; //para el bcd_sevenseg y el fibinario
    logic [6:0] sevenSeg;
    logic fib; //salida si es o no fibbinario  
    
    top_module_sesion DUT(
        .clk(clk),
        .reset(reset),
    //    .BCD_in(BCD_in),
        .sevenSeg(sevenSeg),
        .fib(fib)
    );
    always #5 clk=~clk;  //reloj cambia entre 1-0 cada 5
    initial begin
        clk=0;
        reset=1;
        #10 
        reset=0;
    end
      
endmodule

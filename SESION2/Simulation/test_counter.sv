`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Grupo 311
// 
// Create Date: 27.04.2021 00:03:07
// Design Name: 
// Module Name: test_counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_counter();
    logic clk, reset;
    logic [3:0] count;
    counter_4bit DUT(
        .clk(clk),
        .reset(reset),
        .count(count));
    always #5 clk=~clk;  //reloj cambia entre 1-0 cada 5
    initial begin
        clk=0;
        reset=1;
        #10 reset=0;
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.04.2021 22:43:49
// Design Name: 
// Module Name: test_counter_param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_counter_param(

    );
    logic clk,reset;
    logic [7:0]count;
    
    
    counter4bitparam #(
            .SIZE(4)
        )mod1(
            .clk(clk),//instanciar entradas y salidas
            .reset(reset),
            .count(count)//count
        );
        
     counter4bitparam #(
            .SIZE(5)
        )mod2(
            .clk(clk),//instanciar entradas y salidas
            .reset(reset),
            .count(count)//count
        );
       
     counter4bitparam #(
            .SIZE(8)
        )mod3(
            .clk(clk),//instanciar entradas y salidas
            .reset(reset),
            .count(count)//count
        );
    
    
endmodule

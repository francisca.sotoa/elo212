`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:    Grupo 311
// 
// Create Date: 26.04.2021 23:24:31
// Design Name: DUT de bcd a display de 7
// Module Name: test_bcd_display7
// Project Name: dut de 3.3
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_bcd_display7(
    );
    logic [3:0] BCD_in;
    logic [6:0] sevenSeg;
    BCD_to_sevenSeg DUT(
        .BCD_in(BCD_in),
        .sevenSeg(sevenSeg)
    );
    initial begin
        BCD_in=4'd0;
        #3
        BCD_in=4'd1;
        #3
        BCD_in=4'd2;
        #3
        BCD_in=4'd3;
        #3
        BCD_in=4'd4;
        #3
        BCD_in=4'd5;
        #3
        BCD_in=4'd6;
        #3
        BCD_in=4'd7;
        #3
        BCD_in=4'd8;
        #3
        BCD_in=4'd9;
        #3
        BCD_in=4'd10;
        
    end
endmodule

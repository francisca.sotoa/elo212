`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.04.2021 00:38:19
// Design Name: 
// Module Name: counter4bitparam
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter4bitparam #(parameter SIZE=4 )(
    input logic clk,reset,
    output logic [SIZE - 1 :0] count //count
    );
    
    always_ff @(posedge clk) begin
        if (reset)
            count<= 4'b0; //count
        else
            count<=count+1;//count
    end
    /*always_comb begin
        if (reset)
            count= 4'b0; //count
        else
            count=count+1;//count
    end*/
    
    
    
endmodule

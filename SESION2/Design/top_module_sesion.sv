`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.04.2021 20:51:20
// Design Name: 
// Module Name: top_module_sesion
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: Contador y muestreo de numeros, con fibbinario en display
//                  version estandar sin parametros 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_module_sesion(
        input logic clk, reset, //para contador
//        output logic [3:0] count,  
   //     input logic [3:0] BCD_in, //para el bcd_sevenseg y el fibinario
        output logic [6:0] sevenSeg, //salida a display
        output logic fib //salida si es o no fibbinario    
    );
    logic [3:0] count;
    //instancio los modulos
    counter_4bit mod1( 
        .clk(clk),
        .reset(reset),
        .count(count)
    );
    
    BCD_to_sevenSeg mod2(
        .BCD_in(count),
        .sevenSeg(sevenSeg)
    );
    
    fib_rec mod3(
        .BCD_in(count),
        .fib(fib) //o al sevenSeg le asigno el dp en la cadena de bits?
    );
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: GRUPO 311
// 
// Create Date: 10.05.2021 22:43:33
// Design Name: S4_actividad2
// Module Name: S4_actividad2
// Project Name: Sesion4.2
// Target Devices: 
// Tool Versions: 
// Description: 
// Ejercicio para el contador de N bits
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad2#(parameter N=12 )(
	input logic clock, reset, load, dec, enable,    //creamos variables de entrada y salida
	input logic [N-1:0] load_value,
	output logic [N-1:0] counterN
);

    always_ff @(posedge clock) begin
        if(reset == 1'b1) begin //Si el reset esta en 1 el contador toma el valor 0
            //Rest sincronico, setea el contador a un valor conocido
            counterN<= 'd0;
        end else begin  //para cantos de subida del reloj
            if(load == 1'b1 )begin //Si load es 1, entonces el contador toma el valor de load_value
                counterN<= load_value;
            end else begin
                if(enable == 1'b1 )begin //Solo si enable es 1, entonces dec funciona como sumador/restador del counterN
                    if(dec == 1'b0 )begin   //revisamos si incrementa o decrementa
                        counterN <= counterN + 'd1;
                    end else begin
                        counterN <= counterN - 'd1;
                    end
                end 
                
            end
        end //else 
    end
endmodule

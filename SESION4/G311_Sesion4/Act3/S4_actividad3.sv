`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// Engineer: Grupo 311
// 
// Create Date: 11.05.2021 23:35:36
// Design Name:     diseño actividad 3 sesion 4
// Module Name: S4_actividad3
// Project Name: ALU basica
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad3 #(parameter N=4)(
    input  logic [N-1:0]   A, B,    //declaracion de entradas y salidas del modulo
	input  logic [1:0]   OpCode,
	output logic [N-1:0]   Result,
	output logic [3:0]   Status  //{N,Z,C,V}

    );
    logic [N:0] flag;   //variable interna de tamaño N+1 que guarda el resultado de operacion y el carry
    always_comb begin
        case(OpCode)
            2'b00: begin    //caso desuma 
                        flag=A+B;   //resultado de suma incluyendo carry out
                        Result=flag[N-1:0]; //asigno el resultado de la suma sin el bit de carry
                        if(flag[N]==1'b1)begin //Para revisar si hay carry out
                            Status[1]=1'b1;//C=1    //asigno el carry
                        end
                        else begin
                            Status[1]=1'b0;//C=0
                        end
                        
                        //Suma de A+B Sistema con signo
                        if(((A[N-1]& B[N-1]& ~Result[N-1])| (~A[N-1]& ~B[N-1]& Result[N-1]))==1  )begin //revisamos los bits mas significativos
                            Status[0]=1'b1;// Para revisar si hay overflow, V=1
                        end
                        else begin
                            Status[0]=1'b0;//V=0
                        end
             
                   end
            2'b01: begin       //para restar
                        flag=A-B;   //resultado de resta incluyendo carry 
                        Result=flag[N-1:0]; //asigno el resultado sin el bit de carry
                        //Restade A-B Sistema sin signo
                        if(flag[N]==1'b1)begin  //reviso si hay carry
                            Status[1]=1'b1;//C=1
                        end
                        else begin
                            Status[1]=1'b0;//C=0
                        end
                        
                        //Resta de A-B Sistema con signo
                        if(((~A[N-1]& B[N-1]& Result[N-1])|(A[N-1]& ~B[N-1]& ~Result[N-1]))==1 )begin
                            Status[0]=1'b1; //V=1
                        end
                        else begin
                            Status[0]=1'b0; //V=0
                        end
             
                   end
            2'b10: begin
                        Result= A|B;    //operacion or
                        Status[1:0]=2'b00; //apago el C y V
                   end 
            
            2'b11: begin
                        Result= A&B;    //operacion and
                        Status[1:0]=2'b00;
                   end  
        endcase
        
        if(Result=='b0)begin //Si el resultado de la operacion es 0 , Z=1
            Status[2]=1'b1;
        end
        else begin
            Status[2]=1'b0;
        end
        
        if(Result[N-1]==1'b1)begin //Asumimos que el bit mas significativo es 1 para negativo y 0 para nos va a dar un numero negativo
            Status[3]=1'b1;
        end
        else begin
            Status[3]=1'b0;//Si es positivo se setea N=0
        end
        

    
    
    
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2021 00:52:08
// Design Name: 
// Module Name: deco_binario
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module deco_binario(
    input logic [2:0] A, 
    output logic [7:0]    anodos 
    
    );
    logic [2:0]sel; 
    //Deco binario para seleccionar el display a mostrar
    always_comb begin
        sel=A;    
        case(sel)
            3'b000 : anodos=~8'b00000001;
            3'b001 : anodos=~8'b00000010;    
            3'b010 : anodos=~8'b00000100;
            3'b011 : anodos=~8'b00001000;
            3'b100 : anodos=~8'b00010000;
            3'b101 : anodos=~8'b00100000;      
            3'b110 : anodos=~8'b01000000;
            3'b111 : anodos=~8'b10000000;
            default : anodos=~8'b00000000;
        endcase
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// Engineer: GRUPO 311
// 
// Create Date: 09.05.2021 23:10:04
// Design Name: pASAR NUMERO DE BCD_IN A DISPLAY DE 7 SEGMENTOS
// Module Name: to_sevenSeg
// Project Name: Actividad 1 sesion 4
// Target Devices: 
// Tool Versions: 
// Description: recibimos un numero de 4 bits en hexadecimal y se pasa para mostrar en el display de 7 segmentos
// 
//////////////////////////////////////////////////////////////////////////////////

//Modulo para pasar la entrada BCD_in en un display de 7 segmentos
module to_sevenSeg(
    
    input logic [3:0] BCD_in,
    
    output logic [6:0] sevenSeg
    );
    
    always_comb begin          
                case (BCD_in)
                    4'd0: sevenSeg = ~7'b1111110;//0 abcdefg
                    4'd1: sevenSeg = ~7'b0110000;//1
                    4'd2: sevenSeg = ~7'b1101101;//2
                    4'd3: sevenSeg = ~7'b1111001;//3
                    4'd4: sevenSeg = ~7'b0110011;//4
                    4'd5: sevenSeg = ~7'b1011011;//5
                    4'd6: sevenSeg = ~7'b1011111;//6
                    4'd7: sevenSeg = ~7'b1110000;//7
                    4'd8: sevenSeg = ~7'b1111111;//8
                    4'd9: sevenSeg = ~7'b1110011;//9
                    
                    4'd10: sevenSeg = ~7'b1110111;//A
                    4'd11: sevenSeg = ~7'b0011111;//b
                    4'd12: sevenSeg = ~7'b1001110;//C
                    4'd13: sevenSeg = ~7'b0111101;//d
                    4'd14: sevenSeg = ~7'b1001111;//E
                    4'd15: sevenSeg = ~7'b1000111;//F
                    
                    
                    default: sevenSeg=~7'b0000000;
                endcase
     end
            
    
    
    
    
    
    
    
    
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 13:07:24
// Design Name: 
// Module Name: test_reg1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_reg1();
    logic in, clk,  reset, load,  out;
    
    reg1 DUT(
        .in(in),
        .clk(clk),
        .reset(reset),
        .load(load),
        .out(out));
        
  always #1 clk = ~clk;
  
  initial begin
  #5
  clk=0;
  in=0;
  reset=0;
  load=0;
  
  #5
  in=0;
  reset=0;
  load=1;
  
  #5
  in=0;
  reset=1;
  load=0;
  
  #5
  in=0;
  reset=1;
  load=0;
  
  #5
  in=0;
  reset=1;
  load=1;
  
  #5
  in=1;
  reset=0;
  load=0;
  
  #5
  in=1;
  reset=0;
  load=1;
  
  #5
  in=1;
  reset=1;
  load=0;
  
  #5
  in=1;
  reset=1;
  load=1;
  #3
  reset=0;
  end
    
    
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 15:42:36
// Design Name: 
// Module Name: test_reg3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module  test_reg3();
    logic clk,ShiftIn,enable,reset, ShiftOut;
    
    reg3 DUT(
        
        .clk(clk),
        .ShiftIn(ShiftIn),
        .enable(enable),
        .reset(reset),
        .ShiftOut(ShiftOut)
        );
   always #1 clk = ~clk;
   
   initial begin
   #15 clk=0;
   ShiftIn=0;
   enable=0;
   reset=0;
  
  #15
  ShiftIn=0;
  enable=0;
  reset=1;
  
  #15
  ShiftIn=0;
  enable=1;
  reset=0;
  
  #15
  ShiftIn=0;
  enable=1;
  reset=0;
  
  #55
  ShiftIn=0;
  enable=1;
  reset=1;
  
  #15
  ShiftIn=1;
  enable=0;
  reset=0;
  
  #15
  ShiftIn=1;
  enable=0;
  reset=1;
  
  #15
  ShiftIn=1;
  enable=1;
  reset=0;
  
  #15
  ShiftIn=1;
  enable=1;
  reset=1;
  #3
  reset=0;
  
   
   end
   
endmodule

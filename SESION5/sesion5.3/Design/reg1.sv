`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2021 20:48:23
// Design Name: 
// Module Name: reg1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reg1(input logic in, input logic clk, input logic reset, input logic load, output logic out);
    always_ff @(posedge clk)begin
        if (reset)
            out <= 1'b0;
        else if (load)
            out <= in;
    end 
endmodule

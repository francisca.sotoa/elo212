`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 15:13:44
// Design Name: 
// Module Name: reg3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reg3(
    input logic clk, ShiftIn , enable , reset ,
    output logic ShiftOut);
    logic [31:0] shift_reg;//registro en cual la informacion binaria puede ser guardada y despues 
                            //cambiada a la derecha o izquierda cuando la se�al de control es confirmada
    logic [31:0] shift_next;
    
    
    
    always_comb begin //El shift_reg es reseteado con reset esta activo/1/true
        shift_next = shift_reg;
        if (enable) begin
            shift_next = {shift_reg[30:0],ShiftIn};
        end
        
        //ShiftOut = shift_n[31];
    end 
    
    always_ff @(posedge clk) begin
       if (reset) 
            shift_reg <= 'd0;
       else
           shift_reg<=shift_next; 
       ShiftOut <= shift_next[31];
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2021 20:55:18
// Design Name: 
// Module Name: reg2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reg2(
    input logic clk, ShiftIn , enable , reset ,
    output logic ShiftOut);
    logic [31:0] shift_reg;//registro en cual la informacion binaria puede ser guardada y despues 
                            //cambiada a la derecha o izquierda cuando la se�al de control es confirmada
   
    always_ff @(posedge clk) begin
        if (reset) 
            shift_reg <= 'd0;
        else if (enable)
            shift_reg <= {shift_reg[30:0],ShiftIn};
        else
            shift_reg<=shift_reg;
       ShiftOut <= shift_reg[31];
       
        
        
            
        
        
    end
    
       
       
endmodule

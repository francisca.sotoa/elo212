`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 04:32:04
// Design Name: 
// Module Name: Hex_to_sevenSeg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Hex_to_sevenSeg#(parameter N=32 ) (
	input  logic [N-1:0]  BCD_in,
	input  logic 	      clock, reset,
	output logic [6:0]    segments, //{CA, CB, CC, CD, CE, CF,CG}
	output logic [7:0]    anodos   //{AN7, AN6, AN5, AN4, AN3, AN2, AN1, AN0}
);

//NOTAR QUE DEBE PARAMETRIZAR LA(S) ENTRADA(S)

    //Se�ales internas para hacer la conexion entre modulos
    
        
    logic [2:0] count3bits; //Salida de Contador de 3 bits que va a Decodificador Binario
    logic [2:0] sel;        //Salida de Contador de 3 bits que va hacia multiplexor
    
    logic [6:0] sevenSeg_1;// Salidas de modulo to_sevenSeg con cada display encendido de cada BCD de 4 bits proveniente del numero de 32 bits
    logic [6:0] sevenSeg_2;
    logic [6:0] sevenSeg_3;
    logic [6:0] sevenSeg_4;
    /*logic [6:0] sevenSeg_5;
    logic [6:0] sevenSeg_6;
    logic [6:0] sevenSeg_7;
    logic [6:0] sevenSeg_8;*/
    
       
     counter_param #(//Contador de 3 bits
            .SIZE(3)
        )counter_3bit(
            .clk(clock),//clk_out_1
            .count(count3bits),
            .reset(reset)
            
        );
        
        to_sevenSeg BCD_1(//Conversion de numero de 4 bits en el display de 7 segmentos
            .BCD_in(BCD_in[3:0]),
            .sevenSeg(sevenSeg_1)
            
            
        );
        
        to_sevenSeg BCD_2(
            .BCD_in(BCD_in[7:4]),
            .sevenSeg(sevenSeg_2)
            
            
        );
        
                
        to_sevenSeg BCD_3(
            .BCD_in(BCD_in[11:8]),
            .sevenSeg(sevenSeg_3)
            
            
        );
        to_sevenSeg BCD_4(
            .BCD_in(BCD_in[15:12]),
            .sevenSeg(sevenSeg_4)
            
            
        );
        /*to_sevenSeg BCD_5(
            .BCD_in(BCD_in[19:16]),
            .sevenSeg(sevenSeg_5)
            
            
        );
        to_sevenSeg BCD_6(
            .BCD_in(BCD_in[23:20]),
            .sevenSeg(sevenSeg_6)
            
            
        );
        to_sevenSeg BCD_7(
            .BCD_in(BCD_in[27:24]),
            .sevenSeg(sevenSeg_7)
            
            
        );
        to_sevenSeg BCD_8(
            .BCD_in(BCD_in[31:28]),
            .sevenSeg(sevenSeg_8)
            
            
        );*/
        
        multiplexor mux(//Multiplexor
            .BCD_in_1(sevenSeg_1),
            .BCD_in_2(sevenSeg_2),
            .BCD_in_3(sevenSeg_3),
            .BCD_in_4(sevenSeg_4),
            /*.BCD_in_5(sevenSeg_5),
            .BCD_in_6(sevenSeg_6),
            .BCD_in_7(sevenSeg_7),
            .BCD_in_8(sevenSeg_8),*/
            .sel(count3bits),
            .segments(segments));
        
        deco_binario deco_bi(//Decodificador binario 
            .A(count3bits),
            .anodos(anodos)
            
            );
            
           



    
endmodule

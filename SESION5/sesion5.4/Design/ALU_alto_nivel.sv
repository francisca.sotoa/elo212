`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 04:43:49
// Design Name: 
// Module Name: ALU_alto_nivel
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_alto_nivel#(parameter N=16 ) (
	input  logic [N-1:0]  data_in,
	input  logic load_A , load_B , load_Op , updateRes,
	input logic  clk,reset,
	output logic [6:0]   Segments,
	output logic [7:0]    Anodes,
	output logic [3:0] LEDs //verificar el tama�o del bus de LEDs  

);

//NOTAR QUE DEBE PARAMETRIZAR LA(S) ENTRADA(S)

    //Se�ales internas para hacer la conexion entre modulos
    
        
    //logic [N-1:0]   A, B;
	//logic [1:0]   Op;
	
	logic [N-1:0] Result1;//Salida de ALU
    logic [3:0]   Flags1;  //{N,Z,C,V}
    
    logic [N-1:0] out1;//Salida reg_A y entrada ALU
    logic [N-1:0] out2;//Salida reg_B y entrada ALU 
    logic [1:0] outOp;//Salida reg_Op y entrada ALU
    
    logic [N-1:0] BCD; //parametrizable ?????    
        
        
        reg2 reg_A(
            .clk(clk),
            .ShiftIn(data_in),
            .reset(reset),
            .ShiftOut(out1),
            .load(load_A)//implementar load en reg2
        );
        reg2 reg_B(
            .clk(clk),
            .ShiftIn(data_in),
            .reset(reset),
            .ShiftOut(out2),
            .load(load_B)
        );
        
        reg2 #(.N(2)) reg_Op(
            .clk(clk),
            .ShiftIn(data_in[1:0]),
            .reset(reset),
            .ShiftOut(outOp),
            .load(load_Op)
        );
        
        
        
        
        ALU #(.N(16))alu_central(
            .A(out1),
            .B(out2),
            .OpCode(outOp),
            .Result(Result1),
            .Status(Flags1)
        
        
        );
        
        
        
        
        
        reg2 reg_Result(
            .clk(clk),
            .ShiftIn(Result1),
            .reset(reset),
            .ShiftOut(BCD),//BCD parametrizable ???
            .load(updateRes)
        );
        
        reg2 #(.N(4))reg_Flags(
            .clk(clk),
            .ShiftIn(Flags1),
            .reset(reset),
            .ShiftOut(LEDs),
            .load(updateRes)
        );    
        
        
        
        
        
        Hex_to_sevenSeg#(.N(16)) HtSg(
            .BCD_in(BCD),//BCD parametrizable ??
            .clock(clk),
            .reset(reset),
            .segments(Segments),
            .anodos(Anodes)
        
        
        );   



    
endmodule


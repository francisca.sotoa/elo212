`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.06.2021 04:20:32
// Design Name: 
// Module Name: reg2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Arreglar reg2 para que tenga load y tambien cambiar por el reg oficial y bueno porfavor
module reg2 #(parameter N=16)(
    input logic [N-1:0] ShiftIn,
    input logic clk, reset ,load, 
    output logic [N-1:0]ShiftOut);
    
    logic  [N-1:0]temp_out;
        
    always_comb begin 
        if (load) 
            temp_out=ShiftIn;
        else
            temp_out=ShiftOut;
        
    end 
    
    always_ff @(posedge clk) begin
        if (reset)
            ShiftOut<= 'd0;
        else
            ShiftOut<=temp_out;
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.06.2021 15:54:41
// Design Name: 
// Module Name: test_alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
op_code: 00 = A+B
            01= A-B
            10= A|B
            11=A&B
*/
module test_alu(

    );
    logic [15:0] data_in;              
    logic load_A,load_B,load_Op,updateRes;  
    logic clk; 
    logic reset;                       
    logic [6:0] Segments;                 
    logic [7:0] Anodes;                  
    logic [3:0] LEDs; //verificar el tamao deendmodule
    ALU_alto_nivel #(.N(16))DUT(
        .data_in(data_in),
        .load_A(load_A),
        .load_B(load_B),
        .load_Op(load_Op),
        .updateRes(updateRes),
        .clk(clk),
        .reset(reset),
        .Segments(Segments),
        .Anodes(Anodes),
        .LEDs(LEDs)
    );
    always #5 clk=~clk;
    
    
    initial begin
    reset= 0;
    clk= 0;
    data_in= 16'b1110011100010000; 
    
    load_A= 1;
    load_B= 0;
    load_Op= 1;
    updateRes= 0;
    #5 reset=1;
    #5 reset=0;
    #10
    load_A= 1;
    load_B=1;
    load_Op= 1;
    updateRes=1;
 //de aqui los 16 casos:   
    #10 //0000
    load_A=0;
    load_B=0;
    load_Op= 0;
    updateRes=0;
    
    #10 //0001
    load_A= 0;
    load_B= 0;
    load_Op= 0;
    updateRes=1;
    
    #10 //0010
    load_A= 0;
    load_B= 0;
    load_Op= 1;
    updateRes= 0;
    #10 //0011
    load_A= 0;
    load_B= 0;
    load_Op= 1;
    updateRes=1;
     data_in= 16'b1110011100010001; 
    #10 //0100
    load_A=0;
    load_B=1;
    load_Op= 0;
    updateRes=0;
    #10 //0101
    load_A=0;
    load_B=1;
    load_Op=0;
    updateRes=1;
    #10 //0110 6
    load_A=0;
    load_B=1;
    load_Op= 1;
    updateRes=0;
     data_in= 16'b1110011100010010; 
    #10 //7
    load_A=0;
    load_B=1;
    load_Op=1;
    updateRes=1;
    #10 //8
    load_A=1;
    load_B=0;
    load_Op=0;
    updateRes=0;
    #10 //9
    load_A=1;
    load_B=0;
    load_Op=0;
    updateRes=1;
    #10 //10
    load_A=1;
    load_B=0;
    load_Op=1;
    updateRes=0;
     data_in= 16'b1110011100010011; 
    #10 //11
    load_A=1;
    load_B=0;
    load_Op=1;
    updateRes=1;
    #10 //12
    load_A=1;
    load_B=1;
    load_Op=0;
    updateRes=0;
    #10 //13
    load_A=1;
    load_B=1;
    load_Op=0;
    updateRes=1;
    #10 //14
    load_A=1;
    load_B=1;
    load_Op=1;
    updateRes=0;
    #10 //15
    load_A=1;
    load_B=1;
    load_Op= 1;
    updateRes=1;
    end
endmodule    

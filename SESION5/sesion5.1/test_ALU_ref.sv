`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.06.2021 23:22:15
// Design Name: 
// Module Name: test_ALU_ref
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_ALU_ref();
     logic [3:0]   A, B;
	 logic [1:0]   OpCode;
	 logic [3:0]   Result;
	 logic [3:0]   Status;
	 
	 ALU_ref #(.C_WIDTH(4))DUT(
	   .A(A),
	   .B(B),
	   .OpCode(OpCode),
	   .Result(Result),
	   .Status(Status)
	
	
	
	);
	
	initial begin
	    #3
	    A=4'b1110;
	    B=4'b0011;
	    #3OpCode=00;
	    #3OpCode=01;
	    #3OpCode=10;
	    #3OpCode=11;
	   
	   
	   #3A=4'b0001;
	   B=4'b1010;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b0001;
	   B=4'b0110;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b1100;
	   B=4'b1010;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b0110;
	   B=4'b0111;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b1101;
	   B=4'b1010;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b0101;
	   B=4'b0000;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b0011;
	   B=4'b1001;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b1011;//numeros iguales
	   B=4'b1011;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b1111;//numeros iguales mas grandes
	   B=4'b1111;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   #3A=4'b0000;//0�s
	   B=4'b0000;
	   #3 OpCode=00;
	   #3 OpCode=01;
	   #3 OpCode=10;
	   #3 OpCode=11;
	   
	   
	   
	   //restar dos numeros iguales y cosas que den 0, elegir casos extremos
	end
	 
	 
	 
	 
	  
endmodule

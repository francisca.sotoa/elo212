`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2021 12:09:11
// Design Name: 
// Module Name: counter2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter2#(parameter N=8)(
    input logic clock,reset,
    output logic [N-1:0] counter
    );
    logic [N-1:0] counter_next2;
    
    always_comb begin
        counter_next2=counter + 'd1;
    end
    
    always_ff @(posedge clock) begin 
        if(reset)
            counter<= 'd0;
        else
           counter<=counter_next2;
    end
endmodule

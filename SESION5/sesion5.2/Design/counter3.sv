`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2021 12:16:57
// Design Name: 
// Module Name: counter3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter3#(parameter N=8)(
    input logic clock,reset,
    output logic [N-1:0] counter
    );
    logic [N-1:0] counter_next3;
    always_comb begin
        if(reset)
            counter_next3= 'd0;
        else 
            counter_next3= counter + 'd1;
    end
    
    always_ff @(posedge clock) begin
        counter <= counter_next3;
    end
endmodule

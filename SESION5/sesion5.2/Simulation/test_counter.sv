`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.06.2021 18:03:28
// Design Name: 
// Module Name: test_counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_counter();
     logic clock,reset;
    
    
    logic [3:0] count1;
    logic [4:0] count2;
    logic [7:0] count3;
      counter1 #(
            .N(4)//SIZE 4
        )mod1(
            .clock(clock),//instanciar entradas y salidas
            .reset(reset),
            .counter(count1)//count
        );
     
     counter2 #(
            .N(5)//SIZE 5
        )mod2(
            .clock(clock),//instanciar entradas y salidas
            .reset(reset),
            .counter(count2)//count
        );
      
     counter3 #(
            .N(8) //SIZE 8
        )mod3(
            .clock(clock),//instanciar entradas y salidas
            .reset(reset),
            .counter(count3)//count
        );
    
    always #5 clock = ~clock; //Generacion senal de reloj
    //assign BCD_in=count;
    initial begin
        clock=0;
        #1reset=1;
        #10 reset =0;
        #5 reset=1;
        #6 reset=0;
        #1 reset=1;
        #7 reset=0;
        #20 reset=1;
        #15 reset=0;
        #30 reset=1;
        #5 reset=0;
        #9 reset=1;
        #50 reset=0;
        #3 reset=1;
        #20 reset=0;
        #30 reset=1;
        #50 reset=0;
        #1 reset=1;
        #1 reset=0;
        #1 reset=1;
        #1 reset=0;
        #2 reset=1;
        #10 reset=0;
    end
endmodule
